package com.services.leadingpoint.lpservices;

import android.app.PendingIntent;

/**
 * Created by salahalawadi on 5/9/18.
 */

public interface NotificationCallBack {
    PendingIntent onActionIntent(ResponseModel responseModel);
}
