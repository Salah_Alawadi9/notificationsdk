package com.services.leadingpoint.lpservices;


import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by salahalawadi on 4/19/18.
 */

public class NotificationModel {


    @SerializedName("CommonNotificationId")
    private String notificationID;
    @SerializedName("NotificationStatus")
    private NotificationType notificationStatus;
    @SerializedName("DeviceToken")
    private String deviceToken;


    public NotificationModel() {

    }

    public NotificationModel(String notificationID, NotificationType notificationStatus , String deviceToken) {
        this.notificationID = notificationID;
        this.notificationStatus = notificationStatus;
        this.deviceToken = deviceToken;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }

    public NotificationType getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationType notificationStatus) {
        this.notificationStatus = notificationStatus;
    }


    @Override
    public String toString() {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }
}
