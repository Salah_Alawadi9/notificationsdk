package com.services.leadingpoint.lpservices;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.v4.app.NotificationCompat;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by salahalawadi on 5/9/18.
 */

public class NotificationHandler {



    private int smallIcon = R.drawable.notification_empty;
    private int priority = Notification.PRIORITY_MAX;
    boolean autoCancel = true;
    private Bitmap largeIcon = null;

    public static NotificationHandler builder(){
        return new NotificationHandler();
    }

    public ResponseModel showNotification(Context mContext, Map<String, String> payLoad, NotificationCallBack callBack) {
//            JSONObject json = new JSONObject(remoteMessage.getData().toString());
        ResponseModel responseModel = null;
        try {
//            JSONObject json = new JSONObject(payLoad);
            JSONObject data = new JSONObject(payLoad.get("data"));

            String title = data.getString("Title");
            String body = data.getString("Content");
            String notificationId = data.getString("notificationId");
            String type = data.getString("Type");
            String action = data.getString("Action");
            int scene = data.getInt("Scene");
            responseModel = new ResponseModel(title  , body ,notificationId , type , action , scene , Action.valueOf(action));
            responseModel.setActionEnum(Action.valueOf(action));
            NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
            bigText.bigText(body);
            bigText.setSummaryText(type);


            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, "notify_001")
                    .setSmallIcon(smallIcon) // notification icon
                    .setContentTitle(title) // title for notification
                    .setContentText(body) // message for notification
                    .setPriority(priority)
                    .setStyle(bigText)
                    .setAutoCancel(autoCancel); // clear notification after click


            if (largeIcon != null) {
                mBuilder.setLargeIcon(largeIcon);
            }


//            Intent intent = new Intent(mContext, SplashScreenActivity.class);
//            PendingIntent pi = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent pi = callBack.onActionIntent(responseModel);

            mBuilder.setContentIntent(pi);
            NotificationManager mNotificationManager =
                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel("notify_001",
                        "ChannelTitle",
                        NotificationManager.IMPORTANCE_DEFAULT);
                assert mNotificationManager != null;
                mNotificationManager.createNotificationChannel(channel);
            }

            assert mNotificationManager != null;
            mNotificationManager.notify(0, mBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return responseModel;
    }


    public NotificationHandler setLargeIcon(Bitmap largeIcon) {
        this.largeIcon = largeIcon;
        return this;
    }

    public NotificationHandler setAutoCancel(boolean autoCancel) {
        this.autoCancel = autoCancel;
        return this;
    }

    public NotificationHandler setSmallIcon(@DrawableRes int smallIcon) {
        this.smallIcon = smallIcon;
        return this;
    }

    public NotificationHandler setNotificationPriority(int priority) {
        if (priority > -3 && priority < 3)
            this.priority = priority;
        else
            this.priority = Notification.PRIORITY_MAX;
        return this;
    }


}
