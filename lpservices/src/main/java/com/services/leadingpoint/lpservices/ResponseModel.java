package com.services.leadingpoint.lpservices;

/**
 * Created by salahalawadi on 5/9/18.
 */

public class ResponseModel {
    private String title;
    private String content;
    private String notificationId;
    private String type;
    private String action;
    private int scene;
    private Action actionEnum;


    public Action getActionEnum() {
        return actionEnum;
    }

    public void setActionEnum(Action actionEnum) {
        this.actionEnum = actionEnum;
    }

    public int getScene() {
        return scene;
    }

    public void setScene(int scene) {
        this.scene = scene;
    }

    public ResponseModel(String title, String content, String notificationId, String type, String action, int scene, Action actionEnum) {
        this.title = title;
        this.content = content;
        this.notificationId = notificationId;
        this.type = type;
        this.action = action;
        this.scene = scene;
        this.actionEnum = actionEnum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
