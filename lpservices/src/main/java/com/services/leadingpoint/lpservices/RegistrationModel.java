package com.services.leadingpoint.lpservices;

import android.support.annotation.StringRes;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by salahalawadi on 4/24/18.
 */

public class RegistrationModel {

    @SerializedName("DeviceId")
    private String deviceId;
    @SerializedName("Token")
    private String tokenId;

    public RegistrationModel() {
    }

    public RegistrationModel(String deviceId, String tokenId) {
        this.deviceId = deviceId;
        this.tokenId = tokenId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }
}
