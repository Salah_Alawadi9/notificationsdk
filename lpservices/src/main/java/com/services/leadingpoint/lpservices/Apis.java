package com.services.leadingpoint.lpservices;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by salahalawadi on 4/10/18.
 */

public interface Apis {

    @POST("CallBackdetails/")
    @FormUrlEncoded
    Call<ResponseBody> openEvent(
            @Field("CommonNotificationId") String title,
            @Field("NotificationStatus") String status,
            @Field("DeviceToken") String device
    );
//

    @POST("CallBackdetails")
    Call<ResponseBody> openEvent(
            @Body NotificationModel body
    );

    //
//
//    @Headers({"Content-Type: application/json", "Accept: application/json", "CallChannel: android", "SDK_Version: 1.0"
//            , "Authorization: Basic Tm90aWZpY2F0aW9uRW5naW5lOk4wdDFmMWNAdDEwbkVuZzFlVjI="})
//    @POST("TokenRegistration")
//    @FormUrlEncoded
//    Call<ResponseBody> registerEvent(@Field("DeviceId") String deviceID, @Field("Token") String token);


//    @Headers({"Content-Type: application/json", "CallChannel: android", "SDK_Version: 1.0"
//            , "Authorization: Basic Tm90aWZpY2F0aW9uRW5naW5lOk4wdDFmMWNAdDEwbkVuZzFlVjI="})
    @POST("TokenRegistration")
    Call<ResponseBody> registerEvent(@Body RegistrationModel body);


//    @POST("TokenRegistration/")
//    Call<ResponseBody> registerEvent(@Body RegistrationModel registrationModel);

}
