package com.services.leadingpoint.lpservices;

/**
 * Created by salahalawadi on 4/19/18.
 */

public enum NotificationType {

    RECEIVE(1),
    OPEN(2);
    int valueInt;

    NotificationType(int value) {
        valueInt = value;
    }

    public int getValueInt() {
        return valueInt;
    }
}
