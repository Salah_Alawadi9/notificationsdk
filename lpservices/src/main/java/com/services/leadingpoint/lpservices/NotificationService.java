package com.services.leadingpoint.lpservices;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.UUID;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okio.Utf8;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.services.leadingpoint.lpservices.ApiURL.MAIN_COLLECTION;

/**
 * Created by salahalawadi on 4/10/18.
 */

public class NotificationService {


    public static final String USER_NAME = "UserName";
    public static final String PASSWORD = "Password";
    private static NotificationService lpService;

    private String userName;
    private String password;
    private Retrofit retrofit;
    private Context context;
    private Apis apis;
    private boolean isTestMode;


    public static NotificationService getInstance() {
        if (lpService == null)
            return lpService = new NotificationService();
        else
            return lpService;
    }


    public void init(Context context, ApiURL.BaseUrl baseUrl, final ApiURL.BuildType buildType) {
        FirebaseApp.initializeApp(context);
        if (retrofit == null || apis == null) {
            this.context = context;
            final ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(context.getString(R.string.waitMsg));
//            progressDialog.show();
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection(MAIN_COLLECTION)
                    .document(baseUrl.name())
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    String URL = task.getResult().getString(buildType.name());
                    String deviceType = "1";
                    userName = task.getResult().getString(USER_NAME);
                    password = task.getResult().getString(PASSWORD);

                    String base = userName + ":" + password;
                    try {
                        base = Base64.encodeToString(base.getBytes("UTF-8"), Base64.DEFAULT).replaceAll("\n", "");
//                        base = URLEncoder.encode(userName + ":" + password, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

                    final String finalBase = base;
                    httpClient.addInterceptor(new Interceptor() {
                        @Override
                        public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                            Request request = chain.request().newBuilder()
//                            "Accept": "application/json",
//                                    "Content-Type": "application/json"
//                                    .header("Accept", "application/json")
                                    .addHeader("Content-Type", "application/json")
                                    .addHeader("Authorization", "Basic " + finalBase)
                                    .addHeader("CallChannel", "android")
                                    .addHeader("SDK_Version", "1.0")
                                    .build();
                            return chain.proceed(request);
                        }
                    });
                    retrofit = new Retrofit.Builder()
//                    .baseUrl("http://192.168.1.62/PushNotificationsTool/api/Notifications/")
//                    .baseUrl("http://10.4.1.41/PushNotificationTool/api/Notifications/") this
                            .baseUrl(URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(httpClient.build())
                            .build();
                    apis = retrofit.create(Apis.class);
                }
            });
        }

    }


    public NotificationService enableTest() {
        isTestMode = true;
//        this.context = context;
        return this;
    }

    public void sendEvent(final NotificationModel notificationModel) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if (notificationModel == null
                        || notificationModel.getNotificationID() == null
                        || notificationModel.getNotificationStatus() == null
                        || notificationModel.getDeviceToken() == null
                        || notificationModel.getDeviceToken().isEmpty()
                        || notificationModel.getNotificationID().isEmpty()
                        || notificationModel.getNotificationStatus().getValueInt() == 0) {
                    throw new IllegalArgumentException("Notification model cannot be null please init the values of notification model");
                }


                if (retrofit != null && apis != null) {
                    Call<ResponseBody> call = apis.openEvent(
                            notificationModel

                    );
//
//                    Call<ResponseBody> call = apis.openEvent(
//                            notificationModel.getNotificationID(),
//                            String.valueOf(notificationModel.getNotificationStatus().getValueInt()),
//                            notificationModel.getDeviceToken()
//
//                            );
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                            if (isTestMode) {
                                try {
                                    if (response.isSuccessful()
                                            && response.code() == 200
                                            && response.message().contains("Success") || (response.body() != null && response.body().string().contains("Success")))

                                        Toast.makeText(context, "Sent Successfully", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(context, "Failed to sent " + response.code() + " " + response.message(), Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    Toast.makeText(context, "Failed " + response.code() + " " + response.message(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                            if (isTestMode) {
                                try {
                                    Toast.makeText(context, t.getCause().getCause().getMessage(), Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    t.printStackTrace();
                                    Toast.makeText(context, "Failed to sent", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });


                } else
                    throw new IllegalArgumentException("You should init NotificationService before calling methods, call init method");

            }
        }, 5000);
    }


    public void registerDevice(final RegistrationModel registrationModel) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if (retrofit != null && apis != null) {
                    Call<ResponseBody> call = apis.registerEvent(
                            registrationModel);
//
//                    Call<ResponseBody> call = apis.registerEvent(
//                            registrationModel.getDeviceId(), registrationModel.getTokenId());


                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                            if (isTestMode) {
                                try {
                                    if (response.isSuccessful()
                                            && response.code() == 200
                                            && response.message().contains("Success") || (response.body() != null && response.body().string().contains("Success")))
                                        Toast.makeText(context, "Sent Successfully", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(context, "Failed to sent " + response.code() + " " + response.message(), Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    Toast.makeText(context, "Failed " + response.code() + " " + response.message(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                            if (isTestMode) {
                                Toast.makeText(context, "Failed to sent", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });


                } else
                    throw new IllegalArgumentException("You should init NotificationService before calling methods, call init method");
            }
        }, 5000);
    }


    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

    public synchronized static String getDeviceID(Context context) {
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();
            }
        }
        return uniqueID;
    }


    public PendingIntent getPendingIntent(ResponseModel responseModel , Class mClass) {
        Intent intent;

        switch (responseModel.getAction()) {
            case "A":
                intent = new Intent(context, mClass);
                break;

            case "B":
                intent = new Intent(context, mClass);
                break;

            case "C":
                intent = new Intent(context, mClass);
                break;

            default:
                intent = new Intent(context, mClass);
                break;
        }
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
