package com.services.leadingpoint.lpservices;

public enum Action {

    NAVAPP("NAVAPP"),
    DONOTH("DONOTH"),
    NAVOUT("NAVOUT");


    private final String action;

    Action(String action) {
        this.action = action;
    }


    public String getActionString() {
        return this.action;
    }
}
