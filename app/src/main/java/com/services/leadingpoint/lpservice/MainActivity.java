package com.services.leadingpoint.lpservice;

import android.app.Notification;
import android.app.PendingIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.services.leadingpoint.lpservices.ApiURL;
import com.services.leadingpoint.lpservices.NotificationCallBack;
import com.services.leadingpoint.lpservices.NotificationHandler;
import com.services.leadingpoint.lpservices.NotificationModel;
import com.services.leadingpoint.lpservices.NotificationService;
import com.services.leadingpoint.lpservices.NotificationType;
import com.services.leadingpoint.lpservices.RegistrationModel;
import com.services.leadingpoint.lpservices.ResponseModel;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//
//        NotificationService
//                .getInstance()
//                .enableTest()
//                .sendEvent(new NotificationModel("notif_1_id" , NotificationType.OPEN , "deviceToken"));


//        NotificationService
//                .getInstance()
//                .init(this,ApiURL.BaseUrl.KSA , ApiURL.BuildType.TESTING);

//
//        NotificationService
//                .getInstance()
//                .enableTest()
//                .registerDevice(new RegistrationModel(NotificationService.getDeviceID(this) ,
//                        FirebaseInstanceId.getInstance().getToken()));
//


        NotificationHandler.builder()
                .setLargeIcon(null)
                .setNotificationPriority(Notification.PRIORITY_MAX)
                .setAutoCancel(true)
                .showNotification(this, new HashMap<String, String>(), new NotificationCallBack() {

                    @Override
                    public PendingIntent onActionIntent(ResponseModel responseModel) {
                       switch (responseModel.getActionEnum()){
                           case NAVAPP:
                              return NotificationService.getInstance().getPendingIntent(responseModel , MainActivity.class);


                           case DONOTH:

                               break;
                           case NAVOUT:

                               break;


                       }
                        return NotificationService.getInstance().getPendingIntent(responseModel , MainActivity.class);
                    }
                });


    }





    public void SendOpen(View view){
//        NotificationService
//                .getInstance()
//                .enableTest()
//                .sendEvent(new NotificationModel("567567567" , NotificationType.OPEN , FirebaseInstanceId.getInstance().getToken()));
    }




    public void DegisterDevice(View view){

//        NotificationService
//                .getInstance()
//                .enableTest()
//                .registerDevice(new RegistrationModel(NotificationService.getDeviceID(this) ,
//                        FirebaseInstanceId.getInstance().getToken()));
    }




    public void ReceiveEvent(View view){
//        NotificationService
//                .getInstance()
//                .enableTest()
//                .sendEvent(new NotificationModel("567567567" , NotificationType.RECEIVE , FirebaseInstanceId.getInstance().getToken()));

    }
}
