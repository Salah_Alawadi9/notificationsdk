package com.services.leadingpoint.lpservice;

import android.app.Application;
import android.content.Context;

//import com.google.firebase.FirebaseApp;
import com.services.leadingpoint.lpservices.ApiURL;
import com.services.leadingpoint.lpservices.NotificationService;

/**
 * Created by salahalawadi on 5/16/18.
 */


public class MyApp extends Application {


    @Override
    public void onCreate() {
        super.onCreate();


    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);





        NotificationService.getInstance().init(base, ApiURL.BaseUrl.KSA , ApiURL.BuildType.TESTING);


    }

}